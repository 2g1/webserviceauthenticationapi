import express from "express";
import cors from "cors";
import movies from "./movies/router.js";
import moviesCategories from './movies_categories/router.js'
import swaggerJsdoc from "swagger-jsdoc";
import swaggerUi from "swagger-ui-express";



const app = express();
app.use(cors());

app.use(movies)
app.use(moviesCategories)

const options = {
  definition: {
    openapi: "3.1.0",
    info: {
      title: "Authentication API",
      version: "1.0.0",
      description:
        "Api d'authentification avec accès token ",
      license: {
          name: "GOMEZ Eve - GRAVE Léa - M1 DEVLMIOT",
        },
    },
    servers: [
      {
        url: "http://localhost:8000",
      },
    ],
  },
  apis: ["./account/router.js", "./token/router.js"],
};

const specs = swaggerJsdoc(options);
app.use(
  "/api-auth",
  swaggerUi.serve,
  swaggerUi.setup(specs)
);



app.listen(3000, () => {  console.log("Connecté à la base de données auth-api de MySQL!")})
